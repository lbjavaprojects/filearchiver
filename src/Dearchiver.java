import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Dearchiver {

	public static void main(String[] args) {
		try(FileInputStream fis = new FileInputStream(args[0]);
				ZipInputStream zip = new ZipInputStream(fis)){
			ZipEntry ze;
			while((ze=zip.getNextEntry())!=null)
			{
				String fileName;
				fileName=ze.getName();
				try(FileOutputStream zop = new FileOutputStream(fileName);){
					int bufferSize=1024,c;
					byte[] buffer=new byte[bufferSize];
					while((c=zip.read(buffer, 0, bufferSize))>-1){
						zop.write(buffer, 0, c);
					}
					
				}System.out.println("Wypakowano plik "+ fileName);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
