import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Archive {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try(FileOutputStream fos=new FileOutputStream("archiwum.zip");
			ZipOutputStream zos=new ZipOutputStream(fos)){
			for(String fileName : args){
			try(FileInputStream fis=new FileInputStream(fileName)){
					
				ZipEntry ze=new ZipEntry(fileName);
				zos.putNextEntry(ze);
				int bufferSize=1024,c;
				byte[] buffer=new byte[bufferSize];
				while((c=fis.read(buffer, 0, bufferSize))>-1){
					zos.write(buffer, 0, c);
				}
				zos.closeEntry();
			}
			System.out.println("Zarchiwizowano plik: "+fileName);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String dropExtention(String filename){
		return filename.substring(0,filename.lastIndexOf("."));
	}
	}
